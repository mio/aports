# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-integration
pkgver=5.26.0
pkgrel=0
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel"
makedepends="
	extra-cmake-modules
	kguiaddons-dev
	kidletime-dev
	kwayland-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e0dcd88ae42a59dceb780eea6666bdf3a152a14ba87f3753f2eb4ed025c765de62c6887c469eb09b0e86c1e73cbe28e244fbbd86bbf1bdd12c06f31e4b7c4426  kwayland-integration-5.26.0.tar.xz
"
