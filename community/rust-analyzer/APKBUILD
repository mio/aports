# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2022.10.10
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-lang/rust-analyzer"
# armhf, armv7, x86: some tests fail, not supported by upstream
# riscv64, s390x: blocked by cargo/rust
arch="aarch64 ppc64le x86_64"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-lang/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"
# requires rustup toolchain to run rustup rustfmt for sourcegen
options="net !check"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rust-analyzer -t "$pkgdir"/usr/bin/
	install -Dm644 docs/user/manual.adoc -t "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
d6703c65bbb6b73e378612919e8ee1a2b3f7d1f9be97e0e3bab91273ccdf7e21449c0b799297027631224a56c85c0942bb35939f56782295c2abaf9fc7434a2d  rust-analyzer-2022.10.10.tar.gz
"
